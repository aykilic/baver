<?php

namespace App\Http\Controllers;

use App\Enums\makbuzad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Models\numaralaObj;
use App\Models\dovizObj;
use App\Models\olayObj;
use App\Models\birimObj;
use App\Models\stokObj;
use App\Models\depoObj;
use App\Models\fisturuObj;
use App\Models\stokturObj;
use App\Models\vergiObj;
use App\Models\sipfisObj;
use App\Models\makbuzObj;

class makbuzController extends Controller
{
    public function makbuzlar(request $req,$makbuzturu)
    {
        $dropdoviz = dovizObj::pluck('dad','did');
//        $makbuzadi = makbuzad::getInstance(makbuzad::Tahsilat);
if($makbuzturu==2){
    $fisturu=7;
}elseif($makbuzturu==1){
    $fisturu=8;
}
//        $userType->key; // SuperAdministrator
//        $userType->value; // 0
//        $userType->description; // Super Administrator
//       dd( makbuzad::Tahsilat);
//        $makbuzturu = (string) $makbuzturu;
//        $makbuzturu = "$makbuzturu";
        if(makbuzad::Tahsilat == $makbuzturu){
        $makbuzadi=makbuzad::getkey(2);
        }else{
            $makbuzadi=makbuzad::getkey(1);
        }
        $sonnumara=app('App\Http\Controllers\irsaliyeController')->sonnumara($fisturu);
//dd($sonnumara);

//dd($makbuzadi);
            return view::make('makbuz.makbuz')
                ->with('makbuzturu',$makbuzturu)
                ->with('doviz',$dropdoviz)
                ->with('sonnumara',$sonnumara)
                ->with('makbuzadi',$makbuzadi)
                ;


        // return Redirect::to('alissiparisfisi');
    }
    public function makbuzfislist(Request $request)
    {
//        $cbid = DB::table('makbuzlar')->where('makbuzlar.makbuzturu',$request->makbuzturu)
//
//            ->value("cbid");
//        $baid=DB::table('cbanka')->where('cbanka.cbid',$cbid)
//
//            ->value("baid");
        $data = DB::table('makbuzlar')->where('makbuzlar.makbuzturu',$request->makbuzturu)

            ->select('makbuzlar.*','firmalar.*','doviz.*','cbanka.*','bankalar.*')
//            ->where('sipfis.olayid','<>', 1)
//            ->whereRaw("users.id BETWEEN 1003 AND 1004")
//                aynı ifade
//            ->where('sipfis.olayid', 5)
            ->leftjoin('firmalar', 'firmalar.fid', '=', 'makbuzlar.makbuzfid')
            ->leftjoin('doviz', 'doviz.did', '=', 'makbuzlar.makbuzdid')
            ->leftjoin('cbanka', 'cbanka.cbid', '=', 'makbuzlar.cbid')
            ->leftjoin('bankalar', 'bankalar.baid', '=', 'makbuzlar.baid')
//            ->leftjoin('sipdurum', 'siparis.durumid', '=', 'sipdurum.sipdurumid')
//            ->leftjoin('musteriler', 'siparis.mid', '=', 'musteriler.mid')
//            ->orderByRaw('YEAR(tarih) ASC, MONTH(tarih) ASC, DAY(tarih) ASC')
            ->get();

//dd($data);
        $json_data = array(
////            "draw"            => intval(""),
////            "recordsTotal"    => intval(""),
////            "recordsFiltered" => intval(""),
            "data"            => $data
        );
        return  response()->json($json_data);
    }
    public function makbuzfisi(Request $request,$makbuzturu,$id)
    {

        $dropdoviz = dovizObj::pluck('dad','did');
        if(makbuzad::Tahsilat == $makbuzturu){
            $makbuzadi=makbuzad::getkey(1);
        }else{
            $makbuzadi=makbuzad::getkey(2);
        }
        return view::make('makbuz.makbuzfisi')
            ->with('makbuzturu',$makbuzturu)
            ->with('doviz',$dropdoviz)
            ->with('makbuzadi',$makbuzadi)
            ;
    }
    public function makbanka(Request $request)
    {
//        $data=1;
        $data = DB::table('firmalar')->where('firmalar.fid',$request->fisfid)
            ->select('bankalar.baid','bankalar.bankad')
            ->leftjoin('cbanka', 'cbanka.fid', '=', 'firmalar.fid')
            ->leftjoin('bankalar', 'bankalar.baid', '=', 'cbanka.baid')
            ->groupby('bankalar.baid')
            ->get()->toArray();
        return  response()->json($data);
    }
    public function makbankahesap(Request $request)
    {
//        $data=1;
        $data = DB::table('firmalar')
            ->select('cbanka.cbid','cbanka.hesapno','doviz.dad')
            ->where('firmalar.fid',$request->fisfid)
            ->where('cbanka.baid',$request->bankaid)

            ->leftjoin('cbanka', 'cbanka.fid', '=', 'firmalar.fid')
            ->leftjoin('bankalar', 'bankalar.baid', '=', 'cbanka.baid')
            ->leftjoin('doviz', 'doviz.did', '=', 'cbanka.did')
            ->get()->toarray();
        return  response()->json($data);
    }
    public function makbuziptal(request $request)
    {
        if ($request->ajax()) {
            $data = makbuzObj::find($request->makbuzid);
            $data->makdurum = 2;
            $data->save();

            return response()->json($data);
        }


    }

    public function makkaydet(request $request)
    {

        if ($request->ajax()) {
            if($request->makbuzturu==2){
                $fisturu=7;
            }elseif($request->makbuzturu==1){
                $fisturu=8;
            }
            if($request->makbuzid){
                $data =makbuzObj::find($request->makbuzid);
                $data->makbuzfid = $request->fisfid;
                $data->makbuztar = $request->maktar;
                $data->makbuznumara = $request->makbuzno;
                $data->makbuzturu = $request->makbuzturu;
                $data->odemeturu = $request->odemeturu;
                $data->cbid = $request->hesap;
                $data->baid = $request->baid;
                $data->makbuzdid = $request->doviz;
                $data->makbuztutar = $request->tutar;
                $data->save();
            }else{

            $novarmi = DB::table('makbuzlar')->where('makbuznumara',$request->makbuzno)->count();
            if($novarmi!=0){
                }
                else{
                $data = new makbuzObj;


                $data->makbuzfid = $request->fisfid;
                $data->makbuztar = $request->maktar;
                $data->makbuznumara = $request->makbuzno;
                $data->makbuzturu = $request->makbuzturu;
                $data->odemeturu = $request->odemeturu;
                $data->cbid = $request->hesap;
                $data->baid = $request->baid;
                $data->makbuzdid = $request->doviz;
                $data->makbuztutar = $request->tutar;

                $numaralaObj=numaralaObj::find($fisturu);
                $numaralaObj->sonnumara=$request->makbuzno;
                $numaralaObj->save();
                $data->save();
                }
            }



            return response()->json($data);



        }
    }
}
