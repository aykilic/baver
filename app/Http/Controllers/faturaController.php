<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

use App\Http\Requests;
use App\Models\numaralaObj;
use App\Models\dovizObj;
use App\Models\olayObj;
use App\Models\birimObj;
use App\Models\stokObj;
use App\Models\depoObj;
use App\Models\fisturuObj;
use App\Models\stokturObj;
use App\Models\vergiObj;
use App\Models\sipfisObj;
use App\Models\sipfissatirObj;

class faturaController extends Controller
{
    public function faturafisi(request $req, $id = null)
    {
        if($id==null){$idd=$req->fisturu;}
        //
        $firma = DB::table('firmalar')->get();
        $stok = DB::table('stok')->get();
        $firmam = DB::table('firmalar')->select('cunvan')->get();
        //$stokm = DB::table('stoklar')->select('sad')->get();
        //$firmaz=$firmam->toArray();
        $firmay=json_encode($firmam);
        //$stoky=json_encode($stokm);
        //$firmaz=$firmam;
        //$firmaz= firmaObj::pluck('cunvan');
        //$firmaz=$firmam->toJson();
//dd($stok);
        $selectedvergi = vergiObj::first()->vid;
        $fistur = fisturuObj::pluck('fisturuad','fisturuid');
        $dropvergim = vergiObj::pluck('vor','vid');
        $dropbirim = birimObj::pluck('bad','bid');
        $dropdoviz = dovizObj::pluck('dad','did');
        $dropdepo = depoObj::pluck('depoad','depoid');
//       $sipfistur = sipfisiObj::all();
        $olayy = olayObj::pluck('olayad','olayid');
        //$fid = DB::table('firmalar')->where('cunvan', $request->cunvan)->value('fid');
        //$uzunluk=DB::table('numarala')->where('evrakturuid', $id)->value('uzunluk');
        //$index=1;
        //$hane=6;
//        $new_index = str_pad($index, $uzunluk, "0", STR_PAD_LEFT);
        // id 1 satış
//        $numarala=numaralaObj::all();
//        $sipfisno = sipfisObj::where('fisturu', $id)->orderBy('sipfisid', 'desc')
//            ->first();
        if($id==5){
            $idd=1;

        }elseif($id==6){
            $idd=2;
        }elseif($idd==1){$id=5;}elseif($idd==2){$id=6;}
        $sipfisnoadiid=DB::table('fisturu')->select('fisturuad')->where('fisturuid', $idd)->first();
        $sipfisnoadi=$sipfisnoadiid->fisturuad;
        // dd($sipfisnoadi);
        $numarala=numaralaObj::where('evrakturuid', $id)->first();
        $hane=$numarala->uzunluk;
// dd($hane);
        //eğer numaralama aktif değilse
//                    if($numarala->mod==0 || $numarala->mod==""){

        if ($id==5){
            $sonnumm=$numarala->sonnumara + 1;
            $sonnummm=str_pad($sonnumm, $hane, "0", STR_PAD_LEFT);
            $numara=$sonnummm;
        }
        elseif ($id==6){
            $sonnumm=$numarala->sonnumara + 1;
            $sonnummm=str_pad($sonnumm, $hane, "0", STR_PAD_LEFT);
            $numara=$sonnummm;
        }
        $olayid=4;
        return View::make('alsat.fatura.faturafis')
            ->with('fistur', $fistur)
            ->with('firma', $firma)
            ->with('numara', $numara)
            ->with('fisturu',$idd)
            ->with('firmay', $firmay)
            ->with('depo', $dropdepo)
//            ->with('vergi', $dropvergi)
            ->with('vergim', $dropvergim)
            ->with('olay', $olayy)
            ->with('olayid', $olayid)
            ->with('svergi', $selectedvergi)
            ->with('stok', $stok)
            ->with('doviz', $dropdoviz)
            ->with('birim', $dropbirim)
            ->with('sipfisnoadi', $sipfisnoadi);
//
    }
    public function faturafisleri(request $req, $fisturu){

        if($fisturu==5){
            $idd='1';
        }else {
            $idd='2';
        }

        $sipfisnoadiid=DB::table('fisturu')->select('fisturuad')->where('fisturuid', $idd)->first();
        $sipfisnoadi=$sipfisnoadiid->fisturuad;
        return View::make('alsat.fatura.fatura')

            ->with('fisturu',$idd)
            ->with('sipfisnoadi', $sipfisnoadi);

    }
    public function faturafislist(Request $request)
    {
        $data = DB::table('sipfis')->where('sipfis.fisturu',$request->fisturu)
            ->groupBy('fatnumara')
            ->select('sipfis.*','firmalar.*','olay.*','doviz.*')
//            ->where('sipfis.olayid','<>', 1)
//            ->whereRaw("users.id BETWEEN 1003 AND 1004")
//                aynı ifade
            ->where('sipfis.olayid', 5)
            ->leftjoin('firmalar', 'firmalar.fid', '=', 'sipfis.fisfid')

            ->leftjoin('olay', 'olay.olayid', '=', 'sipfis.olayid')
            ->leftjoin('doviz', 'doviz.did', '=', 'sipfis.doviz')
//            ->leftjoin('sipdurum', 'siparis.durumid', '=', 'sipdurum.sipdurumid')
//            ->leftjoin('musteriler', 'siparis.mid', '=', 'musteriler.mid')
//            ->orderByRaw('YEAR(tarih) ASC, MONTH(tarih) ASC, DAY(tarih) ASC')
            ->get();


        $json_data = array(
////            "draw"            => intval(""),
////            "recordsTotal"    => intval(""),
////            "recordsFiltered" => intval(""),
            "data"            => $data
        );
        return  response()->json($json_data);
    }
    public function faturakaydet(request $request)
    {
//$fisturu=0;
        // return View::make('alsat.siparis')->with('fisturu', $fisturu);
        $a=$request->fisturu;
        $satirsay=$request->fissid;

        $para=$request->gtoplam;
        // number_format($para,2,",",".");
        $para=str_replace( ".", "", $para );
        $para=str_replace( ",", ".", $para );
        if($request->fisturu==1){
            $evrakturu=5;
            $irturu=3;
        }else{
            $evrakturu=6;
            $irturu=4;
        }
        if($request->edit != 1) //edit değilse
        {

            $sipfisObj= new sipfisObj;
            $sipfisObj->numara=app('App\Http\Controllers\irsaliyeController')->sonnumara($request->fisturu);
            $sipfisObj->irnumara=app('App\Http\Controllers\irsaliyeController')->sonnumara($irturu);
            $sipfisObj->irtar=$request->tar;
            $sipfisObj->sipfistar=$request->tar;
            $sipfisObj->fattar=$request->tar;
            $sipfisObj->fisturu=$request->fisturu;
            $sipfisObj->fatnumara=$request->fatno;
            $sipfisObj->fisfid=$request->fisfid;
            $sipfisObj->depo=$request->depo;
            $sipfisObj->doviz=$request->did;
            $sipfisObj->olayid=5;
            $sipfisObj->gtoplam=$para;
            $sipfisObj->save();

            $sonsipid=sipfisObj::all()->last()->sipfisid;
            for ($i = 0; $i < count($satirsay); $i++) {
//            $sipfissatirObj =sipfissatirObj::find($request->sipfissatirid[$i]);
                $sipfissatirObj=new sipfissatirObj();
                $sipfissatirObj->fissid = $request->fissid[$i];
                $miktarr=str_replace( ".", "", $request->miktar[$i] );
                $miktarr=str_replace( ",", ".", $miktarr );
                $sipfissatirObj->miktar = $miktarr;
                $sipfissatirObj->birim = $request->birim[$i];
                $bfiyatt=str_replace( ".", "", $request->bfiyat[$i] );
                $bfiyatt=str_replace( ",", ".", $bfiyatt );
                $sipfissatirObj->bfiyat = $bfiyatt;
                $sipfissatirObj->kdv = $request->kdv[$i];
                $tutarr=str_replace( ".", "", $request->tutar[$i] );
                $tutarr=str_replace( ",", ".", $tutarr );
                $sipfissatirObj->tutar = $tutarr;
                $sipfissatirObj->numara=app('App\Http\Controllers\irsaliyeController')->sonnumara($request->fisturu);
                $sipfissatirObj->sipfisid = $sonsipid;
                $sipfissatirObj->save();
            }

            $numaralaObj=numaralaObj::find($evrakturu);
            $numaralaObj->sonnumara=app('App\Http\Controllers\irsaliyeController')->sonnumara($evrakturu);
            $numaralaObj->save();

            $numaralaObj=numaralaObj::find($request->fisturu);
            $numaralaObj->sonnumara=app('App\Http\Controllers\irsaliyeController')->sonnumara($evrakturu);
            $numaralaObj->save();

        }else
        {
            $sipidarrays=explode(",",$request->sipaktarrayf);

            foreach($sipidarrays as $sipidarray)
            {
                $sipfisObj=sipfisObj::find($sipidarray);
                $sipfisObj->fattar=$request->tar;
                $sipfisObj->fatnumara=$request->fatno;
                $sipfisObj->olayid=5;
                $sipfisObj->gtoplam=$para;
                $sipfisObj->save();

            }
            for ($i = 0; $i < count($satirsay); $i++) {
                $sipfissatirObj =sipfissatirObj::find($request->sipfissatirid[$i]);
                $sipfissatirObj->fissid = $request->fissid[$i];
                $miktarr=str_replace( ".", "", $request->miktar[$i] );
                $miktarr=str_replace( ",", ".", $miktarr );
                $sipfissatirObj->miktar = $miktarr;
                $sipfissatirObj->birim = $request->birim[$i];
                $bfiyatt=str_replace( ".", "", $request->bfiyat[$i] );
                $bfiyatt=str_replace( ",", ".", $bfiyatt );
                $sipfissatirObj->bfiyat = $bfiyatt;
                $sipfissatirObj->kdv = $request->kdv[$i];
                $tutarr=str_replace( ".", "", $request->tutar[$i] );
                $tutarr=str_replace( ",", ".", $tutarr );
                $sipfissatirObj->tutar = $tutarr;
                $sipfissatirObj->save();
            }


            $numaralaObj=numaralaObj::find($evrakturu);
//            $numaralaObj->sonnumara=$this->sonnumara($evrakturu);
            $numaralaObj->sonnumara=app('App\Http\Controllers\irsaliyeController')->sonnumara($evrakturu);
            $numaralaObj->save();

        }

        if($request->fisturu == 2){


            return redirect('/faturalist/6')
//
                ;
//

        }else{
            return redirect('/faturalist/5')
//
                ;
//

        }

    }
    public function faturaedit(request $request, $fatnumara, $fisturum)
    {

//         $sipnobul=sipfisObj::where('irnumara', $irnumara)->select('sipfisid')->get();
//        $sipnobuls = DB::table('sipfis')->where('fatnumara', $fatnumara)->select('sipfisid')->get();

//        if($fisturum==3){$fisturu=1;}else{$fisturu=2;}

        $sipfis=DB::table('sipfis')->where('fatnumara', $fatnumara)
//            ->select('sipfis.sipfisid')
            ->leftjoin('firmalar','firmalar.fid','=','sipfis.fisfid')
            ->first();

        $sipfissat = DB::table('sipfissatir')                       //birden çok aynı irnolu satırları listele
        ->leftjoin('stok','stok.sid','=','sipfissatir.fissid')
            ->whereIn('sipfissatir.sipfisid',
                function ($query) use ($fatnumara) {
                    $query->from('sipfis')->select('sipfis.sipfisid')
                        ->where('sipfis.fatnumara', $fatnumara);
                })->get();

        $fisturu=DB::table('fisturu')->select('fisturuad')->where('fisturuid', $fisturum)->first();
        $sipfisnoadi=$fisturu->fisturuad;



        $olay= olayObj::pluck('olayad','olayid');
        $depo=DB::table('depo')->get();
        $doviz=DB::table('doviz')->get();
        $dropvergi = vergiObj::all();

        $dropbirim = birimObj::pluck('bad','bid');
        $dropvergim = vergiObj::pluck('vor','vid');

        return View::make('alsat.fatura.faturaedit')
            ->with('fisturuid', $fisturum)
            ->with('sipfis', $sipfis)
            ->with('sipfisid', $sipfis->sipfisid)
            ->with('sipfissat', $sipfissat)
            ->with('fisturu',$fisturu->fisturuad)
            ->with('olay', $olay)
            ->with('secolay', $sipfis->olayid)
            ->with('depo', $depo)
            ->with('secdepo', $sipfis->depo)
            ->with('doviz', $doviz)
            ->with('secdoviz', $sipfis->doviz)
            ->with('birim', $dropbirim)
            //->with('secbirim', $dropbirim)
            ->with('vergim', $dropvergim)
            ->with('numara', $fatnumara)
            ->with('sipfisnoadi', $sipfisnoadi);

    }
    public function faturaeditkaydet(request $request)
    {
//        $sipfisObjj = DB::table('sipfis')->select('sipfisid')->where('irnumara', $request->irno)->get();

        $fatnumara = $request->fatno;
        $satirsay=$request->fissid;
//        $snumara=$sipfisids = DB::table('sipfis')                       //aynı id den birden fazla olursa
//        ->select('sipfis.numara')
//            ->whereIn('sipfis.sipfisid',
//                function ($query) use ($fatnumara) {
//                    $query->from('sipfis')->select('sipfis.sipfisid')
//                        ->where('sipfis.fatnumara', $fatnumara);
//                })->value('numara');
//        dd($irnumara);
//        $sipfisids = [];
//        foreach($sipfisObjj as $sipfisObjs)
//        {
////            array_push($sipfisids, $sipfisObjs->sipfisid); //dizi içindeki idleri sıralama
//////            $fisturu = DB::table('sipfis')->where('irnumara', $sipfisObjs->sipfisid)->first();
////            dd($request->irno);
//
//            $sipfisObj=sipfisObj::find($sipfisObjs->sipfisid);
////            $sipfisObj = DB::table('sipfis')->where('sipfis.sipfisid', $sipfisObjs->sipfisid)->get();
//            $sipfisObj->irnumara=$irnumara;
//            $sipfisObj->irtar=$request->tar;
//            $sipfisObj->save();
//    }
//
        $sipfisids = DB::table('sipfis')                       //aynı id den birden fazla olursa
        ->select('sipfis.sipfisid')
            ->whereIn('sipfis.sipfisid',
                function ($query) use ($fatnumara) {
                    $query->from('sipfis')->select('sipfis.sipfisid')
                        ->where('sipfis.fatnumara', $fatnumara);
                })->pluck('sipfisid')->toarray();

        foreach($sipfisids as $sipfisid)
        {
//            dd($sipfisid);
            $sipfisObj=sipfisObj::find($sipfisid);
            $sipfisObj->fisfid=$request->fisfid;
            $sipfisObj->fatnumara=$request->fatno;
            $sipfisObj->fattar=$request->tar;
            $sipfisObj->depo=$request->depo;
//            $sipfisObj->doviz=$request->did;
//            $sipfisObj->gtoplam=$para;
            $sipfisObj->save();

////            array_push($competition_all, $sipfisid); //dizi içindeki idleri sıralama
        }
//        dd($sipfisids);
//
//        $sipfisObj->save();
//        $satirsay=$request->fissid;
//
        for ($i = 0; $i < count($satirsay); $i++) {
            $sipfissatirObj = sipfissatirObj::find($request->sipfissatirid[$i]);
            $sipfissatirObj->fissid = $request->fissid[$i];
            $miktarr = str_replace(".", "", $request->miktar[$i]);
            $miktarr = str_replace(",", ".", $miktarr);
            $sipfissatirObj->miktar = $miktarr;
            $sipfissatirObj->birim = $request->birim[$i];
            $bfiyatt = str_replace(".", "", $request->bfiyat[$i]);
            $bfiyatt = str_replace(",", ".", $bfiyatt);
            $sipfissatirObj->bfiyat = $bfiyatt;
            $sipfissatirObj->kdv = $request->kdv[$i];
            $tutarr = str_replace(".", "", $request->tutar[$i]);
            $tutarr = str_replace(",", ".", $tutarr);
            $sipfissatirObj->tutar = $tutarr;
            $sipfissatirObj->save();

        }
        if($request->fisturu == 2){


            return redirect('/faturalist/6')
//
                ;
//

        }else{
            return redirect('/faturalist/5')
//
                ;
//

        }
    }
}
