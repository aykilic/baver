<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="Shortcut Icon" href="{{ asset("images/favoicon2.png") }}" type="image/x-icon">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>BaverKimya </title>

    <!-- Bootstrap -->
{{--    <link href="{{ asset("css/bootstrap.min.css") }}" rel="stylesheet">--}}
{{--    <link href="{{ asset("css/font-awesome.min.css") }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/nprogress.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/green.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/pnotify.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/pnotify.buttons.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/normalize.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/buttons.bootstrap.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/scroller.bootstrap.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/custom.min.css') }}" rel="stylesheet">--}}
{{--    <link href="{{asset('css/site.css') }}" rel="stylesheet">--}}
{{--    <link rel="stylesheet" href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css">--}}

    @stack('stylesheets')

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">


        @yield('main_container')

    </div>
</div>

{{--<!-- jQuery -->--}}
{{--<script src="{{ asset("js/jquery.min.js") }}"></script>--}}

{{--<!-- Bootstrap -->--}}
{{--<script src="{{ asset("js/bootstrap.min.js") }}"></script>--}}
{{--<script src="{{ asset("js/jquery.dataTables.min.js") }}"></script>--}}


{{--<script src="{{ asset("js/gentelella.min.js") }}"></script>--}}
{{--<!-- Custom Theme Scripts -->--}}
<!-- jQuery -->

{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>--}}
{{--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--}}
{{--<script src="{{ asset("js/jquery.min.js") }}"></script>--}}

{{--<!-- Bootstrap -->--}}
{{--<script src="{{ asset("js/bootstrap.min.js") }}"></script>--}}
{{--<!-- FastClick -->--}}
{{--<script src="{{ asset("js/jquery.dataTables.min.js") }}"></script>--}}
{{--<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js"></script>--}}
{{--<script src="{{ asset("js/jquery.dataTables.min.js") }}"></script>--}}

{{--<script src="{{ asset("js/moment.min.js") }}"></script>--}}

{{--<script src="{{ asset("js/fastclick.js") }}"></script>--}}
{{--<script src="{{ asset("js/skycons.js") }}"></script>--}}
{{--<!-- inputmask -->--}}

{{--<!-- NProgress -->--}}

{{--<script src="{{ asset("js/jquery.inputmask.bundle.js") }}"></script>--}}
{{--<script src="{{ asset("js/jquery.inputmask.js") }}"></script>--}}
{{--<script src="{{ asset("js/jquery.inputmask.extensions.js") }}"></script>--}}
{{--<script src="{{ asset("js/jquery.inputmask.numeric.extensions.js") }}"></script>--}}



{{--<script src="{{ asset("js/nprogress.js") }}"></script>--}}
{{--<script src="{{ asset("js/daterangepicker.js") }}"></script>--}}

{{--<script src="{{ asset("js/validator.js") }}"></script>--}}
{{--<script src="{{ asset("js/pnotify.js") }}"></script>--}}
{{--<script src="{{ asset("js/pnotify.buttons.js") }}"></script>--}}

{{--<script src="{{ asset("js/pnotify.nonblock.js") }}"></script>--}}
{{--<script src="{{ asset("js/typeahead.js") }}"></script>--}}
{{--<script src="{{ asset("js/custom.min.js") }}"></script>--}}

{{--/-------başla-///////////////////////--}}

{{-----------------------------bitirrrrrr/--}}
<!-- Custom Theme Scripts -->
{{--<script src="{{ asset("js/jquery.min.js") }}"></script>--}}
{{--<script src="{{ asset("js/bootstrap.min.js") }}"></script>--}}
{{--<script src="{{ asset("js/fastclick.js") }}"></script>--}}
{{--<script src="{{ asset("js/nprogress.js") }}"></script>--}}
{{--<script src="{{ asset("js/validator.js") }}"></script>--}}
{{--<script src="{{ asset("js/custom.min.js") }}"></script>--}}
{{--<script src="http://gentelella.app/assets/js/validator.js"></script>--}}




<!-- Datatables -->


@yield('content_script')
</body>
</html>