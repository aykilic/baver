
@extends('layouts.blank')
@push('stylesheets')
@endpush
@section('main_container')

    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    {{--<script src="{{ asset("js/jquery.js") }}"></script>--}}
    {{--<link href="{{ asset("css/site.css") }}" rel="stylesheet">--}}
    <!-- DataTables -->
        <!-- Bootstrap JavaScript -->
        <!-- App scripts -->

        <!-- Bootstrap CSS -->
{{--        <link href="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css" rel="stylesheet">--}}
{{--        <script src="https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js"></script>--}}
        <link href="{{asset("css/mdcw/material.min.css") }}" rel="stylesheet">
        <script src="{{ asset("js/mdcw/material.min.js") }}"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    </head>

    <div class="right_col" role="main">
        <div class="row">
            <div >
                <div class="x_panel">
                    <div class="modall"></div>
                    <div class="clearfix"></div>
                    <div class="x_content">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                {{--@if($makbuzturu==1)--}}
                                {{--<div class="col-md-2">--}}
                                {{--<a href="#" class="btn btn-app btn-hareket">--}}
                                {{--<i class="fa fa-plus-square-o"></i> Ekle--}}
                                {{--</a>--}}
                                {{--</div>--}}
                                {{--@else--}}
                                <div tabindex="0" class="ripple-demo-box ripple-demo-box--secondary mdc-ripple-upgraded ekle" >
{{--                                    <a href="javascript:" class="btn btn-app btn-hareket">--}}
{{--                                        <i class="fa fa-plus-square-o"></i> Ekle--}}
{{--                                    </a>--}}
                                    <i class="mdc-fab__icon material-icons">add</i><span class="mdc-fab__label">EKLE</span>
                                </div>

                                <div id="app"><h1>798798798</h1> </div>
                                <h1>798798798</h1>
                                {{--                                <button class="mdc-fab demo-fab-shaped--three mdc-fab--extended mdc-ripple-upgraded"data-mdc-auto-init="MDCRipple"  aria-label="Create" >--}}
{{--                                    <i class="mdc-fab__icon material-icons">add</i><span class="mdc-fab__label">Create</span></button>--}}
                                {{--@endif--}}







                              

                                <div class="h-20"></div>

                                <div class="clearfix"></div>


                                {{--</form>--}}

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">

{{--                        @if($makbuzturu==1)--}}
{{--                            <h2 class="sat">{{$makbuzadi}} Fişi Listesi </h2>--}}
{{--                        @else--}}
{{--                            <h2 class="al">{{$makbuzadi}} Fişi Listesi </h2>--}}
{{--                        @endif--}}
                        <div class="clearfix"></div>
                        <div class="x_content" style="margin-top:15px">


                            <table class="table table-striped table-bordered jambo_table" id="makbuztablo">
                                <thead>

                                <tr class="headings">

                                    <th >Numara </th>
                                    <th >Tarih </th>
                                    <th>Firma</th>
                                    <th>Ödeme Türü</th>
                                    <th>Banka</th>
                                    <th>Fiş Tutarı</th>
                                    <th>İptal</th>

                                </tr>

                                </thead>
                                {{ csrf_field() }}
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <div class="right_col" role="main">--}}

{{--                    <div class="modall">--}}



{{--                    </div>--}}

{{--                        --}}{{--<form id="formkaydet" action="{{ action('Controller@bankakaydet') }}" method="POST" >--}}





{{--                        {{ csrf_field() }}--}}





{{--        <br>--}}
{{--        <br>--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-12 col-sm-12 col-xs-12">--}}
{{--                <div class="x_panel">--}}

{{--                    <div class="text-field-container">--}}
{{--                        <div class="mdc-text-field text-field mdc-text-field--outlined mdc-text-field--with-leading-icon" data-mdc-auto-init="MDCTextField">--}}
{{--                            <i class="material-icons mdc-text-field__icon">event</i><input type="text" id="text-field-outlined-leading" class="mdc-text-field__input">--}}
{{--                            <div class="mdc-notched-outline mdc-notched-outline--upgraded">--}}
{{--                                <div class="mdc-notched-outline__leading">--}}

{{--                                </div>--}}
{{--                                <div class="mdc-notched-outline__notch" style="">--}}
{{--                                    <label class="mdc-floating-label" for="text-field-outlined-leading" style="">Standarsdfsdffsd</label>--}}
{{--                                </div>--}}
{{--                                <div class="mdc-notched-outline__trailing">--}}

{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                    </div>--}}


{{--                    <div class="clearfix"></div>--}}
{{--                    <div class="x_content" >--}}



{{--                    </div>--}}


{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}


{{--    </div>--}}


{{--    <!-- footer content -->--}}
{{--    <footer>--}}


{{--        <div class="pull-right">--}}
{{--            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlibaaaa3333</a>--}}
{{--        </div>--}}
{{--        <div class="clearfix"></div>--}}
{{--    </footer>--}}
@endsection
@section('content_script')
{{--    <script src="{{ asset("js/icheck.min.js") }}"></script>--}}
{{--    <script src="{{ asset("js/dataTables.bootstrap.min.js") }}"></script>--}}
{{--    <script src="{{ asset("js/dataTables.buttons.min.js") }}"></script>--}}
{{--    <script src="{{ asset("js/responsive.bootstrap.js") }}"></script>--}}
{{--    <script src="{{ asset("js/vfs_fonts.js") }}"></script>--}}
{{--    <script src="{{ asset("js/daterangepicker.tr.js") }}"></script>--}}
{{--    <script src="{{ asset("js/flatpickr.js") }}"></script>--}}
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>


    <script type="text/javascript">
        mdc.autoInit();
        const buttons = document.querySelectorAll('.ekle');
        for (const button of buttons) {
            mdc.ripple.MDCRipple.attachTo(button);
        }
        // $body = $("body");
        // $(document).on({
        //     ajaxStart: function() { $body.addClass("loading");},
        //     ajaxStop: function() { $body.removeClass("loading");}
        // });
       // const axios = require('axios');

        // Make a request for a user with a given ID
        var table=$('#makbuztablo').DataTable({

            "ajax": {
                "url": "/makbuzfislist",
                "type": "post",
                "data":{
                    'makbuzturu':2  //hata
                }
            },
            "columns": [
                {   "data": null,
                    render:function (data) {

                        return "<a id='editn' href='javascript:' data-makbuzid='" + data.makbuzid +"' data-fisfid='" + data.makbuzfid +"' data-fisfad='" + data.cunvan +"' data-maktarih='" + data.makbuztar +"' data-makbuznumara='" + data.makbuznumara +"' data-doviz='" + data.makbuzdid +"' data-tutar='" + data.makbuztutar +"' data-odemeturu='" + data.odemeturu +"' data-bankad='" + data.baid +"' data-hesapd='" + data.cbid +"'>" + data.makbuznumara + "</a>";

                    }
                },
                {   "data": "makbuztar"},


                {   "data": null,
                    render:function (data) {
                        return "<a id='editc' href='javascript:' data-makbuzid='" + data.makbuzid +"' data-fisfid='" + data.makbuzfid +"' data-fisfad='" + data.cunvan +"' data-maktarih='" + data.makbuztar +"' data-makbuznumara='" + data.makbuznumara +"' data-doviz='" + data.makbuzdid +"' data-tutar='" + data.makbuztutar +"' data-odemeturu='" + data.odemeturu +"' data-bankad='" + data.baid +"' data-hesapd='" + data.cbid +"'>" + data.cunvan + "</a>";
                    }
                },
                {   "data": "odemeturu"},
                {
                    "data":null,

                    render: function (data, type, row) {
                        if(data.bankad){
                            return data.bankad + ' : ' + data.hesapno +' '+data.dad;
                        }else{
                            return data=null;}

                    }
                },
                {
                    "data":null,

                    render: function (data, type, row) {
                        var numm = $.fn.dataTable.render.number('.', ',', 2).display(data.makbuztutar);
                        return  numm +' '+data.dad ;
                    }
                },


                {
                    "data": "makdurum",
                    // render:function (data) {
                    //     return "<a class='' href='/makbuz/iptal/"+data.makbuzid +"' id='satiriptal' ><i class='fa fa-trash fa-2x' ></i></a>";
                    // }
                },
            ],

            "order": [[ 1, "desc" ],[ 0, "desc" ]],
            'columnDefs': [



                {
                    'targets': 1,
                    render:function(data){
                        return moment(data).format('DD-MM-YYYY');
                    }
                },


                { "targets": 3,"width": "8%",
                    'className':'dt-body-center',
                    "createdCell": function(td, cellData, rowData, row, col) {

                        switch(cellData || rowData) {
                            case 1:
                                // $(td).addClass('label label-info');

                                $(td).html("<SPAN class='label label-success'  id='acik'>NAKİT</SPAN>");
                                break;
                            case 2:
                                $(td).html("<span class='label label-warning'  >HAVALE</span>");
                                break;
                            // case 4:
                            //     $(td).html("<span class='label label-success'>BANKA</span>");
                            //     break;

                        }
                    }
                },
                {
                    'targets': 5,
                    'className':'dt-body-right'
                },
                {
                    'targets': 6,"width": "5%",
                    'className':'dt-body-center',
                    "createdCell": function(td, cellData, rowData, row, col) {

                        switch(cellData || rowData) {
                            case 1:
                                // $(td).addClass('label label-info');

                                $(td).html("<a data-makbuzid="+rowData.makbuzid +" href='#' id='satiriptal' ><i class='fa fa-times-circle-o fa-2x' ></i></a>");
                                break;
                            case 2:
                                $(td).html("<span class='label label-danger'  >İPTAL</span>");
                                break;
                            // case 4:
                            //     $(td).html("<span class='label label-success'>BANKA</span>");
                            //     break;

                        }
                    }
                },
            ],
            "autoWidth": true,
            "paging": false,
//              "bStateSave": true,
            "language":  {
                "sDecimal":        ",",
                "sEmptyTable":     "Tabloda herhangi bir veri mevcut değil",
                "sInfo":           "_TOTAL_ kayıttan _START_ - _END_ arasındaki kayıtlar gösteriliyor",
                "sInfoEmpty":      "Kayıt bulunamadı",
                "sInfoFiltered":   "(_MAX_ kayıt içerisinden bulunan)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ".",
                "sLengthMenu":     "Sayfada _MENU_ kayıt göster",
                "sLoadingRecords": "Yükleniyor...",
                "sProcessing":     "İşleniyor...",
                "sSearch":         "Ara:",
                "sZeroRecords":    "Eşleşen kayıt bulunamadı",
                "oPaginate": {
                    "sFirst":    "İlk",
                    "sLast":     "Son",
                    "sNext":     "Sonraki",
                    "sPrevious": "Önceki"
                },
                "oAria": {
                    "sSortAscending":  ": artan sütun sıralamasını aktifleştir",
                    "sSortDescending": ": azalan sütun sıralamasını aktifleştir"
                },
                "select": {
                    "rows": {
                        "_": "%d kayıt seçildi",
                        "0": "",
                        "1": "1 kayıt seçildi"
                    }
                }
            }

        });
        axios.get('/firmalar')
            .then(function (response) {
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .finally(function () {
                axios.get('/firmalar')
                    .then(function (response) {
                        console.log("sdfsdfsfds");
                    })
            });
        // const app = new Vue({
        //     el: '#app'
        // });
        // $(document).ready(function() {

            // mdc.textField.MDCTextField.attachTo(document.querySelector('.mdc-text-field'));

        // [].slice.call(document.querySelectorAll('.mdc-text-field')).forEach((ele) => mdc.ripple.MDCRipple.attachTo(ele));
        // mdc.ripple.MDCRipple.attachTo(document.querySelector('.mdc-text-field'));
        // const mdcFoo = require('mdc-foo');
        // const MDCFoo = mdcFoo.MDCFoo;
        // const MDCFooFoundation = mdcFoo.MDCFooFoundation;
        // // const MDCFoo = mdc.foo.MDCFoo;
        // // const MDCFooFoundation = mdc.foo.MDCFooFoundation;
        // const textFields = [].map.call(document.querySelectorAll('.mdc-text-field'), function(el) {
        //     return new MDCTextField(el);
        // });
            // $("#irtarih").flatpickr(
            //     {
            //         //enableTime:true,
            //         altInput: true,
            //         altFormat: "d-m-Y",
            //         dateFormat: "Y-m-d",
            //         weekNumbers: true,
            //         locale:'tr',
            //         allowInput: true,
            //         defaultDate: "today"
            //     }
            //
            // );



//   **************         büyük harf ****************/////////////////////



            // $(":input").keyup(function(){
            //     this.value = this.value.toUpperCase();
            // });
        // });

    </script>
@endsection